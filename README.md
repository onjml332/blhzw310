#第一财经JJ经典租号（JJ金商租号）drz
#### 介绍
JJ经典租号（JJ金商租号）【溦:844825】，JJ经典租号（JJ金商租号）【溦:844825】，　　好诗！新颖幽美的情势，高超洗练的笔墨，诚恳深沉喧闹的情绪，畅快潇洒的情怀，加之优美精典的神韵，给人以心旷神怡激动磅礴的美感。拜读，进修。
　　临走时，指着家中仅剩下的一小袋小米和一个南瓜对奶奶说：“娘，这点小米您和娃省着吃，一定要活着等我们回来，咱熬到麦子熟了就有活路了。”奶奶那年得了浮肿病，小腿肿得像发糕，一按一个深坑，半天都起不来。爹娘走后，为了节省这几斤救命粮；奶奶强挣扎着带我去野外寻野菜、摘树叶、挖草根，然后带回家剁巴剁巴掺上把小米给我熬粥喝。每当看到我那副狼吞虎咽的馋样，奶奶总是揉着她那双肿腿叹息说：“唉，啥时能让娃吃顿净米饭就好了……”。更要命的是奶奶由于营养不良，浮肿病也越来越厉害，走不上十步，就得蹲下来喘息好大一阵。为了活命，奶奶每天晚上就偷偷到地里捋把麦苗回来给我熬汤喝，看我皱着眉头喝不下去，便哄着我说：“娃呀，喝吧，睡一觉，你爹就回来了。”
　　走到船埠了，老翁并不领我到岸边，而是拐进一条荒草稠密的小路，说要让我看一看“大伯公”。我说方才仍旧看过，他说“你看到的确定是北坡那一尊，不一律。”说着咱们已钻到一棵宏大无比的大绿荫下，只见树身有一人字形的裂口，形成一个尖顶的小门形势，竟有级级石阶通入，恍若跨入童话。石阶尖端，供着一个小小的神像，铭文为“拿督大伯公”。老翁报告我，“拿督”是马来语，意为“尊者”。从华夏搬来的大伯公冠上了一个马来尊号，也不要一座神庙，把一棵土熟土长的原始巨树看成了神庙，这简直太让我诧异了。老翁说，开初华夏人到了这边，出港打鱼为生，运气凶吉难卜，发端质疑北坡那尊简单华夏化的地盘神大伯公能否能统率得住马来海疆上的风云。所以她们聪明地请出一尊“就地取材”的大伯公，头戴马来名号，背靠扎根巨树，实足变换成一副土著相貌，从树洞里查看着赤道海面上的华人樯帆。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/